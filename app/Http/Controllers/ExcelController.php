<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Excel;
use App\Company;
use DB;

class ExcelController extends Controller
{
    public function showForm()
    {
        return view('excel-form');
    }


    public function importExcelData(Request $request)
    {
        $request->validate([
            'excelFile' =>'required|mimes:xls,xlsx,csv'
        ]);

        $path = $request->file('excelFile')->getRealPath();
        $data =  Excel::load($path)->get();
        
        $data = $data->toArray();

        // dd($data);
        
        $results = [];
        foreach($data as $key => $val)
        {
            $results[] = (object)$val;
            // echo "First Name" . $val['first_name'] . "<br>";
            // echo "Email" . $val['email'] . "<br>";
            // echo "Salary" . $val['salary'] . "<br>";
            // echo "Status" . $val['status'] . "<br>";

            // echo "<br>";
        }
        

        return view('excel-form',compact('results'));

        // code of package 
        // https://docs.laravel-excel.com/2.1/getting-started/
        // tutorial : https://www.webslesson.info/2019/02/import-excel-file-in-laravel.html
    }







    public function showData()
    {
        $results = Company::all();
        return view('excel-export-data',compact('results'));
    }


    public function exportExcelData()
    {
        $company_data = DB::table('companies')->get()->toArray();
        $company_array[] = array('Company Name', 'Address', 'Email', 'Mobile', 'Phone');


        foreach ($company_data as $company) 
        {
            $company_array[] = array(
            'Company Name'      => $company->name,
            'Address'           => $company->address,
            'Email'             => $company->email,
            'Mobile'            => $company->mobile,
            'Phone'             => $company->phone
            );
        }
        Excel::create('Company Data', function ($excel) use ($company_array) 
        {
            $excel->setTitle('Company Data');
            $excel->sheet('Company Data', function ($sheet) use ($company_array) 
            {
                $sheet->fromArray($company_array, null, 'A1', false, false);
            });
        })->download('xlsx');
    



    }


}
